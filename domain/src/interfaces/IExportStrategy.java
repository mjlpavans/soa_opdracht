package interfaces;

public interface IExportStrategy {

    public void export();
}
