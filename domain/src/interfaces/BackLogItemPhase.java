package interfaces;

public enum BackLogItemPhase {
    TODO,
    DOING,
    READY_FOR_TESTING,
    TESTING,
    TESTED,
    DONE
}
