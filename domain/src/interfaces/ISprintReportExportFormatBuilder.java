package interfaces;

public interface ISprintReportExportFormatBuilder {
    public void addHeader(IHeaderOrFooter header);
    public void addFooter(IHeaderOrFooter footer);
    public void addTeamComposition(DevTeamMember[] devTeamMembers);
    public ISprintReportFormat build();
}