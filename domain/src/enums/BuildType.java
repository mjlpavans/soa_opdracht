package enums;

public enum BuildType {
  MAVEN,
  DOTNET,
  ANT,
  GRADDLE,
  JENKINS
}

