package classes;

import interfaces.BackLogItemPhase;

import java.util.ArrayList;

public class BacklogItem {

    private Backlog backlog;
    private SprintBacklog sprintBacklog;
    private Discussion discussion;
    private BackLogItemPhase backlogItemPhase;
    private ArrayList<Activity> activities;
    private DefinitionOfDone definitionOfDone;
    private DevTeamMember developer;
    private Tester tester;


}
